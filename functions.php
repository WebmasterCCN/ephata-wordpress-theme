<?php

require_once __DIR__.'/lib/php/lib.php';

/* ========================================================= */
/*                  LOAD CUSTOM POST TYPES                   */
/* ========================================================= */

require_once_all_regex(get_theme_file_path() . '/custom-post-types/', "");


// when custom post type single pages are not found, uncomment the following line
flush_rewrite_rules( false );

?>