<?php

function create_custom_post_type($name_singular_lowercase, $opt = []) {
    $opt = array_merge([
        'genre' => 'm',
        'icon' => 'dashicons-info',
        'supports' => ['title', 'editor', 'thumbnail', 'custom-fields'],
        'rest' => true,
        'public' => true,

        'taxonomies' => [],
    ], $opt);

    // CPT name in different forms (singular, plural, slug, ...)
    $name_singular_camelcase = ucwords($name_singular_lowercase);
    $name_plural_lowercase = implode(" ", array_map(function($el){return $el."s";}, explode(" ", $name_singular_lowercase)));
    $name_plural_camelcase = implode(" ", array_map(function($el){return $el."s";}, explode(" ", $name_singular_camelcase)));
    $slug = str_replace(" ", "-", remove_accents($name_singular_lowercase)); 

    // French rules
    $tous = ($opt['genre'] == 'm') ? "Tous" : "Toutes";
    $e = ($opt['genre'] == "m") ? "" : "e";
    $nouveau = ($opt['genre'] == "m") ? "nouveau" : "nouvelle";
    $le = ($opt['genre'] == "m") ? "le " : "la ";
    $first_letter_lower = substr($name_singular_lowercase,0,1);
    if (in_array($first_letter_lower, array("a", "e", "i", "o", "u", "y"))) $le = "l'";

    // On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( $name_plural_camelcase, $name_plural_camelcase),
		// Le nom au singulier
		'singular_name'       => _x( $name_singular_camelcase, $name_singular_camelcase),
		// Le libellé affiché dans le menu
		'menu_name'           => __( $name_plural_camelcase),
		// Les différents libellés de l'administration
		'all_items'           => __( $tous.' les '.$name_plural_lowercase),
		'view_item'           => __( 'Voir les ').$name_plural_lowercase,
		'add_new_item'        => __( 'Ajouter un'.$e.' '.$nouveau.' '.$name_singular_lowercase),
		'add_new'             => __( 'Ajouter', 'ccn'),
		'edit_item'           => __( 'Éditer', 'ccn').' '.$le.$name_singular_lowercase,
		'update_item'         => __( 'Modifier', 'ccn').' '.$le.$name_singular_lowercase,
		'search_items'        => __( 'Rechercher', 'ccn').' un'.$e.' '.$name_singular_lowercase,
		'not_found'           => __( 'Non trouvé'.$e, 'ccn'),
		'not_found_in_trash'  => __( 'Non trouvé'.$e, 'ccn').' dans la corbeille',
    );
    
    // On peut définir ici d'autres options pour notre custom post type
	
	$args = array(
		'label'               => __( $name_plural_camelcase ),
		'description'         => __( 'Tout sur les '.$name_plural_lowercase),
		'labels'              => $labels,
        'menu_icon'           => $opt['icon'],
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => $opt['supports'],
		/* 
		* Différentes options supplémentaires
		*/	
		'hierarchical'        => false,
		'public'              => $opt['public'],
		'has_archive'         => true,
        'rewrite'			  => array( 'slug' => $slug),
        'show_in_rest'        => $opt['rest'],

    );
    
    // register post_type
    add_action( 'init', function() use ($slug, $args) {
        register_post_type( $slug, $args );
    }, 0 );


    // register taxonomies for this post type
    if (empty($opt['taxonomies'])) return $slug;
    foreach ($opt['taxonomies'] as $tax_name => $tax_opts) {
        if (!isset($tax_opts['post_types'])) $tax_opts['post_types'] = $slug;
        create_taxonomy($tax_name, $tax_opts);
    }

    return $slug;
}

/**
 * Creates a new taxonomy
 */
function create_taxonomy($taxo_singular_lcase, $opt = []) {
    $opt = array_merge([
        'genre' => 'm',
        'show_column' => true,
        'hierarchical' => false,
        'post_types' => [],
    ], $opt);

    // CPT name in different forms (singular, plural, slug, ...)
    $make_plural = function($str) {
        if (preg_match("/s$/", $str)) return $str;
        return $str."s";
    };
    $name_singular_camelcase = ucwords($taxo_singular_lcase);
    $name_plural_lowercase = implode(" ", array_map($make_plural, explode(" ", $taxo_singular_lcase)));
    $name_plural_camelcase = implode(" ", array_map($make_plural, explode(" ", $name_singular_camelcase)));
    $slug = str_replace(" ", "-", remove_accents($taxo_singular_lcase)); 

    // French rules
    $tous = ($opt['genre'] == 'm') ? "Tous" : "Toutes";
    $e = ($opt['genre'] == "m") ? "" : "e";
    $nouveau = ($opt['genre'] == "m") ? "nouveau" : "nouvelle";
    $le = ($opt['genre'] == "m") ? "le " : "la ";
    $first_letter_lower = substr($taxo_singular_lcase,0,1);
    if (in_array($first_letter_lower, array("a", "e", "i", "o", "u", "y"))) $le = "l'";

    // On déclare ici les différentes dénominations de notre taxonomie qui seront affichées et utilisées dans l'administration de WordPress
    $labels = array(
		'name'              			=> _x( $name_plural_camelcase, 'taxonomy general name'),
		'singular_name'     			=> _x( $name_singular_camelcase, 'taxonomy singular name'),
		'search_items'      			=> __( "Chercher un$e ".$taxo_singular_lcase),
		'all_items'        				=> __( $tous.' les années'),
		'edit_item'         			=> __( 'Éditer '.$le.$taxo_singular_lcase),
		'update_item'       			=> __( 'Mettre à jour '.$le.$taxo_singular_lcase),
		'add_new_item'     				=> __( 'Ajouter un'.$e.' '.$nouveau.' '.$taxo_singular_lcase),
		'new_item_name'     			=> __( 'Valeur de '.$le.$nouveau.' '.$taxo_singular_lcase),
		'separate_items_with_commas'	=> __( 'Séparer les '.$name_plural_lowercase.' avec une virgule'),
		'menu_name'                     => __( $name_singular_camelcase),
    );
    
    $args = array(
    // Si 'hierarchical' est défini à false, notre taxonomie se comportera comme une étiquette standard
        'hierarchical'      => $opt['hierarchical'],
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => $opt['show_column'],
        'query_var'         => true,
        'rewrite'           => array( 'slug' => $slug ),
    );

    // register the taxonomy
    if (!empty($opt['post_types'])) {
        add_action( 'init', function() use ($slug, $opt, $args) {
            register_taxonomy( $slug, $opt['post_types'], $args );
        }, 0);
    }
}

?>