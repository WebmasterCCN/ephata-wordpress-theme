<?php

require_once __DIR__.'/../lib/php/wp-custom-posts.php';

// == 1. == on crée le custom post type 'voyage'
$slug = create_custom_post_type(
    'voyage', 
    [
        'genre' => 'm',
        'supports' => ['title', 'thumbnail', 'custom-fields'],
        'taxonomies' => [
            'pays' => ['hierarchical' => true],
        ],
    ]
);

?>